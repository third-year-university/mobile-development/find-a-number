package ru.dolbak.findnumber

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var minNum = 0
    var maxNum = 0
    private lateinit var textView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        minNum = intent.getIntExtra("min", 0)
        maxNum = intent.getIntExtra("max", 0)
        textView = findViewById(R.id.textView)
        textView.text = ((maxNum + minNum) / 2).toString()
    }

    fun onClick2(view: View) {
        if (maxNum - minNum <= 1){
            textView.text = "Ваше число $minNum"
            textView.setTextColor(Color.GREEN)
        }
        else{
            if (view.id == R.id.button2){
                maxNum = (maxNum + minNum) / 2
            }
            else if (view.id == R.id.button3){
                minNum = (maxNum + minNum) / 2
            }
            textView.text = "${((maxNum + minNum) / 2)}"
        }
    }
}