package ru.dolbak.findnumber

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast

class MainActivity2 : AppCompatActivity() {
    private lateinit var minEdit: EditText
    private lateinit var maxEdit: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        minEdit = findViewById(R.id.editTextNumberSigned)
        maxEdit = findViewById(R.id.editTextNumberSigned2)

    }

    fun onClick(view: View) {
        val minNum = minEdit.text.toString().toInt()
        val maxNum = maxEdit.text.toString().toInt()
        if (minNum >= maxNum){
            Toast.makeText(this, "min должно быть меньше max!!!", Toast.LENGTH_LONG).show()
        }
        else{
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("min", minNum)
            intent.putExtra("max", maxNum)
            startActivity(intent)
        }
    }
}